import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes: routes,
  layout:{
    name: '八维创作平台',
  }
});