export default [
  {
    path: '/',
    component: '@/pages/workbench/index',
    exact: true,
    name: '工作台', // 兼容此写法
    icon: 'DashboardOutlined',
  },
  {
    path: '/article',
    name: '文章管理', 
    icon:'FormOutlined',
    routes: [
      {
        path: '/article/all/all',
        component: '@/pages/article/all/all',
        exact: true,
        name: '所有文章', 
        icon:'FormOutlined'
      },
      {
        path: '/article/classification/classification',
        component: '@/pages/article/classification/classification',
        exact: true,
        name: '分类管理', 
        icon:'FormOutlined'
      },
      {
        path: '/article/label/label',
        component: '@/pages/article/label/label',
        exact: true,
        name: '标签管理', 
        icon:'TagOutlined'
      },
    ],
  },
  {
    path: '/page/page',
    component: '@/pages/page/page',
    exact: true,
    name: '页面管理', 
    icon: 'FileZipOutlined',
  },
  {
    path: '/knowledge/knowledge',
    component: '@/pages/knowledge/knowledge',
    exact: true,
    name: '知识小册', 
    icon: 'FileOutlined',
  },
  {
    path: '/posters/posters',
    component: '@/pages/posters/posters',
    exact: true,
    name: '海报管理', 
    icon: 'StarOutlined',
  },
  {
    path: '/comments/comments',
    component: '@/pages/comments/comments',
    exact: true,
    name: '评论管理', 
    icon: 'MessageOutlined',
  },
  {
    path: '/email/email',
    component: '@/pages/email/email',
    exact: true,
    name: '邮件管理', 
    icon: 'MailOutlined',
  },
  {
    path: '/file/file',
    component: '@/pages/file/file',
    exact: true,
    name: '文件管理',
    icon: 'FolderOpenOutlined',
  },
  {
    path: '/search/search',
    component: '@/pages/search/search',
    exact: true,
    name: '搜索记录', 
    icon: 'SearchOutlined',
  },
  {
    path: '/access/access',
    component: '@/pages/access/access',
    exact: true,
    name: '访问统计', 
    icon: 'ProjectOutlined',
  },
  {
    path: '/user/user',
    component: '@/pages/user/user',
    exact: true,
    name: '用户管理', 
    icon: 'UserOutlined',
  },
  {
    path: '/system/system',
    component: '@/pages/system/system',
    exact: true,
    name: '系统设置', 
    icon: 'SettingOutlined',
  },
];
